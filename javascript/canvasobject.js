var CanvasObject = (function(){
    CanvasObject.prototype.setCanvas = function(pCanvas){
        this.mCanvas = pCanvas;
    };
    CanvasObject.prototype.getCanvas = function(){
        return this.mCanvas;
    };
    // Creates a new dictionary of vector objects to be able to reference.
    CanvasObject.prototype.setDictionaryOfObjects = function(){
        this.mObjectDictionary = new Array();
    };
    CanvasObject.prototype.getDictionaryOfObjects = function(){
        return this.mObjectDictionary;
    };
    // Adds an array of vectors to the dictionary
    CanvasObject.prototype.createNewCanvasObject = function(pName, pVectors){
        this.getDictionaryOfObjects().push({name: pName, vectors: pVectors});
    };
    CanvasObject.prototype.getSpecificCanvasObject = function(pName){
        var obj = null;
        for (var i = 0; i < this.getDictionaryOfObjects().length; i++){
            if (this.getDictionaryOfObjects()[i].name == pName){
                obj = this.getDictionaryOfObjects()[i];
            }
        }
        if (obj != null){
            return obj;
        }else{
            console.log("Object doesn't exist (or is the name wrong?");
        }
    };
    // Draw a specific object in the array on the canvas
    CanvasObject.prototype.drawSpecificObject = function(pName){
        var obj = this.getSpecificCanvasObject(pName).vectors;
        this.getCanvas().drawOnCanvas(obj, 'red');
    };


    function CanvasObject(pCanvas){
        this.setCanvas(pCanvas);
        this.setDictionaryOfObjects();
    }
    return CanvasObject;
})();

