// Check to see if browser supports add event listener function
if(window.addEventListener){
    window.addEventListener(
        'load', // load event
        onLoad, // event handler
        false //useCapture
    );
}

// Define any initial values
var c, cObj;

function initialise(){
    c = new Canvas('canvas', '2d');

    var pVectors = [
        new Vector(10, 10),
        new Vector(100, 10),
        new Vector(100, 100),
        new Vector(10, 100)
    ];

    cObj = new CanvasObject(c);
    cObj.createNewCanvasObject('square', pVectors);

    var newVectors = [
        new Vector(200, 200),
        new Vector(250, 250),
        new Vector(150, 250)
    ];

    cObj.createNewCanvasObject('triangle', newVectors);
}

function draw(){
    c.setBackground('blue');
    cObj.drawSpecificObject('triangle');
    cObj.drawSpecificObject('square');
}

function onLoad(){
    initialise();
    draw();
}
