// Create canvas object
var Canvas = (function(){
    Canvas.prototype.setCanvas = function(pCanvas){
        this.mCanvas = pCanvas;
    };
    Canvas.prototype.getCanvas = function(){
        return this.mCanvas;
    };
    Canvas.prototype.setContext = function(pContext){
        this.mContext = this.getCanvas().getContext(pContext);
    };
    Canvas.prototype.getContext = function(){
        return this.mContext;
    };
    Canvas.prototype.setBackground = function(pBackgroundColour){
        this.getContext().fillStyle = pBackgroundColour;
        this.getContext().fillRect(0, 0, this.getCanvas().width, this.getCanvas().height);
    };
    Canvas.prototype.drawOnCanvas = function(pVectors, fillStyle){
        this.getContext().beginPath();
        this.getContext().fillStyle = fillStyle;
        for (var i = 0; i < pVectors.length; i++){
            // check if its the first vector
            if (i == 0) {
                this.getContext().moveTo(pVectors[i].getX(), pVectors[i].getY());
            }
            else {
                this.getContext().lineTo(pVectors[i].getX(), pVectors[i].getY());
                if (i == (pVectors.length - 1)) {
                    this.getContext().closePath();
                }
            }
        }
        this.getContext().fill();
        this.getContext().stroke();
    };

    function Canvas(pCanvas, pContext){
        // pCanvas is a string that represents the canvas div identifier
        // pContext is a string the represents the dimensions of the canvas (2d, 3d)

        this.setCanvas(document.getElementById(pCanvas));
        if(!this.getCanvas()){
            console.log('User Error: canvas element cannot be found.');
            return;
        }
        if(!this.getCanvas().getContext){
            console.log('Error: canvas cannot get its context.');
            return;
        }
        this.setContext(pContext);
        if(!this.getContext()){
            console.log('Failed to set canvas context to 2d');
            return;
        }
    }
    return Canvas;
})();